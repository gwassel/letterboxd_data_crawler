run:
	dvc repro

build_push:
	docker buildx build --platform linux/amd64,linux/arm/v7,linux/arm64 -t gwassel/letterboxdd --push .

build_push_no_cache:
	docker buildx build --no-cache --platform linux/amd64,linux/arm/v7,linux/arm64 -t gwassel/letterboxdd --push .

clean:
	rm ./film_links_by_year/*.csv && rm ./users/*.csv