import os

import sqlalchemy
from dotenv import load_dotenv
from sqlalchemy import Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
# Base.query = db_session.query_property()
load_dotenv()

DB_LOGIN = os.environ["DB_LOGIN"]
DB_PASSWORD = os.environ["DB_PASSWORD"]
DB_IP = os.environ["DB_IP"]

engine = sqlalchemy.create_engine(
    f"postgresql+psycopg2://{DB_LOGIN}:{DB_PASSWORD}@{DB_IP}:5432/postgres"
)

connection = engine.connect()

metadata = sqlalchemy.MetaData()


class FILM_LINK(Base):
    __tablename__ = "FILM_LINK"

    film_link_id = Column(Integer, primary_key=True)
    film_link = Column(String, unique=True)


class USER_LINK(Base):
    __tablename__ = "USER_LINK"

    user_link_id = Column(Integer, primary_key=True)
    user_link = Column(String)


Base.metadata.create_all(engine)

engine.dispose()
