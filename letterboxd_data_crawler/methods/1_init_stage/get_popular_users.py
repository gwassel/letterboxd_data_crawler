import math
import os
import time
from concurrent.futures import ThreadPoolExecutor

# import lxml
import requests
import sqlalchemy
from bs4 import BeautifulSoup

from letterboxd_data_crawler.models import USER_LINK

THREADS = 128
Q = list()

DB_LOGIN = os.environ["DB_LOGIN"]
DB_PASSWORD = os.environ["DB_PASSWORD"]
DB_IP = os.environ["DB_IP"]

engine = sqlalchemy.create_engine(
    f"postgresql+psycopg2://{DB_LOGIN}:{DB_PASSWORD}@{DB_IP}:5432/postgres"
)
Session = sqlalchemy.orm.sessionmaker(engine)


def link(i):
    return f"https://letterboxd.com/members/popular/this/week/page/{i}"


def get_persons_list(page):
    r = requests.get(link(page))
    soup = BeautifulSoup(r.text, "lxml")
    persons_list = soup.find_all(class_="table-person")

    return persons_list


def get_users_by_page(page):
    persons_list = get_persons_list(page)

    num_persons = len(persons_list)
    if num_persons > 0:
        with Session.begin() as session:
            for person in persons_list:
                person_link = person.find(class_="name").get("href")
                session.add(USER_LINK(user_link=person_link))


def get_num_ppl_in_page(page):
    persons = len(get_persons_list(page))
    return persons


def if_page_alive(page):
    num_ppl = get_num_ppl_in_page(page)
    if num_ppl == 0:
        return page


def find_max_filled_page(l=1, r=10000):
    pages = [2**i for i in range(1, int(math.log2(r)) + 1)]
    with ThreadPoolExecutor(16) as executor:
        results = [i for i in executor.map(if_page_alive, pages) if i]
        return min(results) + 1


if __name__ == "__main__":
    print("run users")
    min_page = 1
    max_page = find_max_filled_page()
    pages = range(min_page, max_page)

    start_time = time.time()
    with ThreadPoolExecutor(64) as executor:
        executor.map(get_users_by_page, pages)
    end_time = time.time()

    print(end_time - start_time)
