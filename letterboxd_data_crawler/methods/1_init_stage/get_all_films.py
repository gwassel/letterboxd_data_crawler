import math
import os
import re
import time
from concurrent.futures import ThreadPoolExecutor

import requests
import sqlalchemy
from bs4 import BeautifulSoup

from letterboxd_data_crawler.models import FILM_LINK

THREADS = 128
Q = list()

DB_LOGIN = os.environ["DB_LOGIN"]
DB_PASSWORD = os.environ["DB_PASSWORD"]
DB_IP = os.environ["DB_IP"]

engine = sqlalchemy.create_engine(
    f"postgresql+psycopg2://{DB_LOGIN}:{DB_PASSWORD}@{DB_IP}:5432/postgres"
)
Session = sqlalchemy.orm.sessionmaker(engine)


def get_number_of_films_by_year(year):
    num_films = get_number_of_films(
        f"https://letterboxd.com/films/ajax/popular/year/{year}/size/small/page/1"
    )
    return num_films


def get_number_of_films(url):
    r = requests.get(url)
    soup = BeautifulSoup(r.text, "lxml")
    str_to_parse = soup.text
    if str_to_parse.find("There are no films") != -1:
        return 0
    else:
        return int("".join(re.findall(r"\d+", str_to_parse)[:-1]))


def get_number_of_pages(num_films):
    FILMS_ON_PAGE = 12 * 6
    return math.ceil(num_films / FILMS_ON_PAGE)


def get_films_from_page(year, page):
    r = requests.get(
        f"https://letterboxd.com/films/ajax/popular/year/{year}/size/small/page/{page}"
    )
    soup = BeautifulSoup(r.text, "lxml")
    tags = soup.find_all("li")
    with Session.begin() as session:
        for tag in tags:
            current_film_link = tag.div.get("data-target-link")
            # if not session.query(FILM_LINK).filter_by(film_link=current_film_link).first():
            session.add(FILM_LINK(film_link=current_film_link))


def put_task(year):
    number_of_films = get_number_of_films_by_year(year)
    number_of_pages = get_number_of_pages(number_of_films)
    for page in range(1, number_of_pages + 1):
        Q.append((year, page))


def fill_queue(l=1800, r=2100):
    years = range(l, r)
    with ThreadPoolExecutor(THREADS) as executor:
        executor.map(put_task, years)


def process_page(year_page):
    year, page = year_page
    get_films_from_page(year, page)


if __name__ == "__main__":
    start = time.time()
    print("fill q")
    fill_queue(1800, 2100)

    print("exec")
    with ThreadPoolExecutor(THREADS) as executor:
        executor.map(process_page, Q)
    temp_end = time.time()
    print(f"end {temp_end - start}")

    engine.dispose()
