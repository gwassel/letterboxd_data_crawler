from sqlalchemy import Column, Integer, String, ForeignKey

from .base import Base

class REVIEW(Base):
    __tablename__ = "REVIEW"

    review_id: int = Column(Integer, primary_key=True)
    review_text: str = Column(Integer, nullable=False)
    user_id: int = Column(Integer, ForeignKey())