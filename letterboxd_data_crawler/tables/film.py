from sqlalchemy import Column, Integer, String, ForeignKey
from .language import LANGUAGE

from .base import Base

class FILM(Base):
    __tablename__ = "FILM"

    film_id: int = Column(Integer, primary_key=True)
    film_inner_id: int = Column(Integer, unique=True, nullable=False)
    year: int = Column(Integer, nullable=False)
    url: str = Column(String, nullable=False, unique=True)
    title: str = Column(String, nullable=False)
    description: str = Column(String)
    poster_url: str = Column(String, nullable=False)
    tagline: str = Column(String)
    original_language: int = Column(String, ForeignKey(LANGUAGE), nullable=True)
    alternative_titles: str = Column(String)
    film_length: int = Column(Integer, nullable=False)

    watches: int = Column(Integer, nullable=False)
    appear_in_lists: int = Column(Integer, nullable=False)
    likes: int = Column(Integer, nullable=False)
    fans: int = Column(Integer, nullable=False)
    
    ratings: str = Column(String, nullable=False)
