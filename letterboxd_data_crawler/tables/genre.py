from sqlalchemy import Column, Integer, String, ForeignKey

from .base import Base

class GENRE(Base):
    __tablename__ = "GENRE"

    genre_id: int = Column(Integer, primary_key=True)
    genre: str = Column(String, nullable=False, unique=True)