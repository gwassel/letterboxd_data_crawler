from sqlalchemy import Column, Integer, String, ForeignKey

from .base import Base

class COUNTRY(Base):
    __tablename__ = "COUNTRY"

    country_id: int = Column(Integer, primary_key=True)
    country: str = Column(String, unique=True, nullable=False)