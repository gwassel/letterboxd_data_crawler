from sqlalchemy import Column, Integer, String, ForeignKey

from .base import Base

class LANGUAGE(Base):
    __tablename__ = "LANGUAGE"

    language_id: int = Column(Integer, primary_key=True)
    language: str = Column(String, nullable=False, unique=True)