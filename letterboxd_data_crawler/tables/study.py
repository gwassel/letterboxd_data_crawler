from sqlalchemy import Column, Integer, String, ForeignKey

from .base import Base

class STUDY(Base):
    __tablename__ = "STUDY"

    study_id: int = Column(Integer, primary_key = True)
    study: str = Column(String, nullable=False, unique=True)