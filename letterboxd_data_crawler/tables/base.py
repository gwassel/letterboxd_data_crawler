from typing import Generator

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import Session, sessionmaker
import os

Base = declarative_base()

DB_LOGIN = os.environ["DB_LOGIN"]
DB_PASSWORD = os.environ["DB_PASSWORD"]
DB_IP = os.environ["DB_IP"]

DB_URL = f"postgresql+psycopg2://{DB_LOGIN}:{DB_PASSWORD}@{DB_IP}:5432/postgres"

class Database:
    def __init__(self) -> None:
        self.engine = create_engine(
            url=DB_URL,
            # pool_size=config.DB_POOL_SIZE,
            # connect_args={"connect_timeout": config.DB_CONNECT_TIMEOUT},
        )

        self.session = sessionmaker(
            autocommit=False,
            autoflush=False,
            bind=self.engine,
        )

    def __call__(self) -> Generator[Session, None, None]:
        session: Session = self.session()

        try:
            yield session
        finally:
            session.close()


DB = Database()
