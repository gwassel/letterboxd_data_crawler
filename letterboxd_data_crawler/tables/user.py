from sqlalchemy import Column, Integer, String, ForeignKey

from .base import Base

class USER(Base):
    __tablename__ = "USER"

    user_id = None
    url = None
    username = None
    favorite_films = None
    films_watched = None
    films_likes = None
    reviews_writed = None
    reviews_likes = None
    lists_created = None
    lists_likes = None
    tags = None
    following = None
    followers = None