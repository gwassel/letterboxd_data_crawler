from sqlalchemy import Column, Integer, String, ForeignKey

from .base import Base

class THEME(Base):
    __tablename__ = "THEME"

    theme_id: int = Column(Integer, primary_key=True)
    theme: str = Column(String, nullable=False, unique=True)