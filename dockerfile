FROM python:3.8-buster AS base

WORKDIR /letterboxd_data_crawler

RUN apt update && apt upgrade -y && apt -y install libpq-dev gcc \
    libffi-dev cython libxml2-dev libxslt-dev build-essential\
    libssl-dev python3-dev cmake wget g++ python3-lxml zlib1g-dev

ENV PIP_DISABLE_PIP_VERSION_CHECK=1 \
    PIP_NO_CACHE_DIR=1

RUN wget https://github.com/libgit2/libgit2/archive/refs/tags/v1.5.0.tar.gz -O libgit2-1.5.0.tar.gz &&\
    tar xzf libgit2-1.5.0.tar.gz &&\
    cd libgit2-1.5.0/ &&\
    cmake . && make && make install && cd .. && rm -rf libgit2-1.5.0/

RUN pip install -U pip && pip install --upgrade --no-cache \
    setuptools wheel lxml requests beautifulsoup4 sqlalchemy psycopg2 python-dotenv dvc cryptography==3.1.1 

COPY . .

CMD ["python", "test_run.py"]